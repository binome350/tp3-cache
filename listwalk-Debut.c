#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <string.h>
#include <assert.h>
#include <math.h>

//#define int int64_t
void swap(int64_t i, int64_t j, int64_t* tab);
void show(int64_t* tab, int64_t N);
void followLinks(int64_t* tab, int64_t N);
void followLinksv2(int64_t* tab, int64_t N);
void shufflev1(int64_t* tab,int64_t N);
void shufflev2(int64_t* tab,int64_t N);
void doTab(int64_t* tab, int64_t N);
void doTabNTimes(int64_t* tab, int64_t N, int64_t R);
void init(int64_t* tab, int64_t N);

int main() {
    //Paramètre and cie.
    srandom(clock());
    int64_t N = 1000000;
    int64_t R = 1000;
    
    //Affchage
    printf("N=%ld\n", N);
    printf("taille tableau=%ld\n", N * sizeof (int64_t));
    
    //Initilisation
    int64_t* tab = (int64_t*) malloc(N * sizeof (int64_t));

    for (int64_t i = 0; i < N; i++) {
        tab[i] = i;
        //DEBUG // printf("i=%ld\n",i);
    }
    
    //Lance les tests
    doTabNTimes(tab,N,R);
    
    //Nettoyage
    free(tab);

    return 0;
}

void init(int64_t* tab, int64_t N){
    tab = (int64_t*) malloc(N * sizeof (int64_t));

    for (int64_t i = 0; i < N; i++) {
        tab[i] = i;
        //DEBUG // printf("i=%ld\n",i);
    }
}

void doTab(int64_t* tab, int64_t N){
    
    //Melange
    shufflev2(tab,N);
    
    double timeIN = clock();
    followLinksv2(tab, N);
    double timeOUT = clock();
    
    double timeTotal = (timeOUT-timeIN)/N;
    
    //Traitement
    int64_t tempsExec = (timeTotal/CLOCKS_PER_SEC)*1000000000;
    printf("tempsExec=%ld ns\n",tempsExec);
}

void doTabNTimes(int64_t* tab, int64_t N, int64_t R){
    //Melange
    shufflev2(tab,N);
    
    double timeIN = clock();
    for(int i = 0; i <R ; i++){
        followLinksv2(tab, N);
    }
    
    double timeOUT = clock();
    double timeTotal = (timeOUT-timeIN);
    
    //Traitement
    int64_t tempsExec = (((timeTotal/CLOCKS_PER_SEC)/R)/N)*1000000000;
    printf("tempsExec=%ld ns\n",tempsExec);
}

void shufflev1(int64_t* tab,int64_t N) {
    for (int64_t i = 0; i < N; i++) {
        swap((int64_t) random() % N, (int64_t) random() % N, tab);
        //Random() renvoit un truc entre 0 et "beaucoup", %N pour revenir sur le bon range.
        //DEBUG // printf("i=%ld\n",i);
    }
}

void shufflev2(int64_t* tab,int64_t N) {
    // Sattolo Cycle Algorithm (voir wikipédia =) )
    
    for (int64_t i = N-1 ; i > 0; i--) {
        //Voir algo
        int64_t rand = random() % i; 
        swap(rand, i, tab);
        //DEBUG // printf("i=%ld\n",i);
    }

}

void swap(int64_t i, int64_t j, int64_t* tab) {
    //printf("i = %ld\n", i);
    //printf("i = %ld\n", j);
    int64_t tmp = tab[i];
    tab[i] = tab[j];
    tab[j] = tmp;
    //printf("SWAP fait\n");
}

void show(int64_t* tab, int64_t N) {
    for (int64_t i = 0; i < N; i++) {
        printf("tab[%ld]=%ld\n", i, tab[i]);
    }
}

void followLinks(int64_t* tab, int64_t N) {

    //La première valeur qu'on visite
    int64_t tmp = tab[0];
    //int64_t caseActuelle = 0;
    //int64_t nbCaseParcourue = 0;
    
    for (int64_t i = 0; i < N; i++) {
        
        //caseActuelle = tmp;
        //nbCaseParcourue++;
        
        tmp = tab[tmp];
        if (tmp == 0) {
            //printf("=> 0 trouvé en case : %ld\n", caseActuelle);
            break;
        }
    }
    
    //printf("=> followLinks : %ld cases parcourues\n", nbCaseParcourue);

}

void followLinksv2(int64_t* tab, int64_t N) {
    
    int64_t tmp = tab[0];
    
    do{
        tmp = tab[tmp];
    } while(tmp != 0);
}


/*
 ANCIEN MAIN : 
 * int main() {
    srandom(clock());
    int64_t N = 10;

    printf("N=%ld\n", N);

    //Initilisation
    int64_t* tab = (int64_t*) malloc(N * sizeof (int64_t));

    for (int64_t i = 0; i < N; i++) {
        tab[i] = i;
        //DEBUG // printf("i=%ld\n",i);
    }
    
    //Affichage avant shuffle
    show(tab, N);

    // SWAP
    //shufflev1(tab,N);
    shufflev2(tab,N);

    //Affichage après shuffle
    show(tab, N);
    //Parcours 
    followLinks(tab, N);
    //Nettoyage
    free(tab);


    return 0;
}
 
 */
